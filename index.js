const buttonSend = document.querySelector('.send')

buttonSend.addEventListener('click', () => {
  const messages = document.querySelector('.messages')
  const text = document.querySelector('#text-message').value

  const li = document.createElement('li')
  li.classList.add('message')

  li.innerHTML = `
    ${text}
    <div class="buttons">
      <button class="edit">EDITAR</button>
      <button onclick="del(this) "class="del">DELETAR</button>
    </div>
    
  `

  messages.appendChild(li)
})


const del = (button) => {
  const parentLi = button.parentElement.parentElement
  parentLi.remove()
}
